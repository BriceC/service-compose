### 1st step : Create all machine 
docker create pi1

docker create pi2

docker create pi3

### 2nd step : docker swarm
eval $(docker-machine env pi1)

docker swarm init --advertise-addr ${pi1_local_ip}

eval $(docker-machine env pi2)

docker swarm join --token ${token} ${pi1_local_ip}:2377

eval $(docker-machine env pi3)

docker swarm join --token ${token} ${pi1_local_ip}:2377

### 3rd step : create a registry on pi1 to store images
eval $(docker-machine env pi1)

docker service create --name registry --publish published=5000,target=5000 registry:2

cd service-compose

docker-compose up --build -d

docker-compose push

### 4th step : deploy the site and the service on the swarm
docker stack deploy --compose-file docker-compose.yml servicecompose

docker service update --replicas=6 servicecompose_service


